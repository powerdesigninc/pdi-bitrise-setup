#!/bin/bash
set -e

check_egynate_access_token() {
  if [ -z "$EGNYTE_ACCESS_TOKEN" ] ; then
    echo -e "\$EGNYTE_ACCESS_TOKEN is required"
    exit 1
  fi
}

# set default values if it is empty
export config_ssh_private_key_path=${config_ssh_private_key_path:-/Shared/IT/Applications/CICD/ssh_private_key}
export config_vpn_cred_path=${config_vpn_cred_path:-/Shared/IT/Applications/CICD/credentials/vpn}
export config_docker_cred_path=${config_docker_cred_path:-/Shared/IT/Applications/CICD/credentials/docker}
export config_db_cred_path=${config_db_cred_path:-/Shared/IT/Applications/CICD/credentials/database}
export config_atlassian_cred_path=${config_atlassian_cred_path:-/Shared/IT/Applications/CICD/credentials/atlassian}
export config_ghostinspector_cred_path=${config_ghostinspector_cred_path:-/Shared/IT/Applications/CICD/credentials/ghostinspector}
export config_smtp_cred_path=${config_smtp_cred_path:-/Shared/IT/Applications/CICD/credentials/smtp}

# install tools


curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
apt-get update 1>/dev/null

export SCRIPT_DIR=$(dirname $0)/scripts

cd $SCRIPT_DIR

if [ "$use_pdi_vpn" == 'yes' ] ; then
  check_egynate_access_token
  ./setup_vpn.sh
fi

if [ "$use_pdi_docker_cluster" == 'yes' ] ; then
  check_egynate_access_token
  ./setup_docker.sh
fi

if [ "$use_pdi_database" == 'yes' ] ; then
  ./setup_database.sh
fi