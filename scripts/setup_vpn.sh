#!/bin/bash
set -e

echo -e "\e[32m# Start setup VPN\e[0m"

./vpn/openconnect.sh

./vpn/verify.sh
