set -e

if [ -z "$1" ] ; then
  echo -e "\e[31m\Download Path is required.\e[0m"
fi

curl --location \
     --fail \
     --silent \
     --show-error \
     --header "Authorization: Bearer $EGNYTE_ACCESS_TOKEN" \
     --output $2 \
     "https://powerdesigninc.egnyte.com/pubapi/v1/fs-content$1"

#echo "$1 is downloaded"
