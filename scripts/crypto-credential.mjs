import * as crypto from 'crypto';
import fs from 'fs';
import { request } from 'https';

if (!process.env['EGNYTE_ACCESS_TOKEN']) {
  console.error('ERROR: EGNYTE_ACCESS_TOKEN is required');
  process.exit(1);
}

const egnyteAccessToken = process.env['EGNYTE_ACCESS_TOKEN'];
const algorithm = 'aes-192-cbc';
const cipherKey = Buffer.from(egnyteAccessToken);
const cipherIv = cipherKey.subarray(0, 16);

if (process.argv[2] == 'encrypt') {
  process.stdout.write(encrypt(readStdin()));
} else if (process.argv[2] == 'decrypt') {
  process.stdout.write(decrypt(readStdin()));
} else if (process.argv[2] == 'print') {
  const file = process.argv[3];

  download(file, function (encrypted) {
    const names = process.argv.slice(4);
    const credential = decrypt(encrypted).split('\n');

    for (let index = 0; index < names.length; index++) {
      const name = names[index];
      const value = credential[index];
      process.stdout.write(`export ${name}='${value}';\n`);
      process.stdout.write(`envman add --key ${name} --value '${value}';\n`);
    }
  });
} else {
  console.error('ERROR: Unknown command');
  process.exit(1);
}

function readStdin() {
  try {
    process.stdin.resume();
    return fs.readFileSync(0).toString();
  } catch (error) {
    console.error('ERROR: Cannot read input');
    process.exit(1);
  }
}

function encrypt(input) {
  const cipher = crypto.createCipheriv(algorithm, cipherKey, cipherIv);
  const encrypted = Buffer.concat([cipher.update(input), cipher.final()]);

  return encrypted.toString('base64');
}

function decrypt(input) {
  const buffer = Buffer.from(input, 'base64');
  const cipher = crypto.createDecipheriv(algorithm, cipherKey, cipherIv);
  const decrypted = Buffer.concat([cipher.update(buffer), cipher.final()]);

  return decrypted.toString('utf8');
}

function download(file, callback) {
  var req = request(
    `https://powerdesigninc.egnyte.com/pubapi/v1/fs-content${file}`,
    {
      headers: {
        Authorization: `Bearer ${egnyteAccessToken}`,
      },
    }
  );

  req.on('error', function (error) {
    console.error('ERROR:', error);
    process.exit(1);
  });

  req.on('response', function (res) {
    var buffer = Buffer.alloc(0);
    res.on('data', function (data) {
      buffer = Buffer.concat([buffer, data]);
    });

    res.on('error', function (error) {
      console.error('ERROR:', error);
      process.exit(1);
    });

    res.on('end', function () {
      var encrypted = buffer.toString();
      if (res.statusCode >= 400) {
        console.error('ERROR:', encrypted);
        process.exit(1);
      } else {
        callback(encrypted);
      }
    });
  });

  req.end();
}
