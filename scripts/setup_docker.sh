#!/bin/bash
set -e

echo -e "\e[32m# Start setup docker cluster\e[0m"

# update docker-compose to latest
curl -L -s --output /usr/local/bin/docker-compose https://github.com/docker/compose/releases/download/1.28.5/docker-compose-`uname -s`-`uname -m`
chmod +x /usr/local/bin/docker-compose

export DOCKER_HOST=unix:///var/run/docker2.sock
envman add --key DOCKER_HOST --value $DOCKER_HOST

# start new docker daemon because the VPN is not working on parent dockerd
dockerd --host=$DOCKER_HOST --storage-driver=vfs &

sleep 5

# decrypt docker credential to username/password
eval `node ./crypto-credential.mjs print $config_docker_cred_path DOCKER_USER DOCKER_PASSWORD`

# docker login
echo "$DOCKER_PASSWORD" | docker login -u $DOCKER_USER --password-stdin docker.powerdesigninc.us 

# add ssh key to ssh-agent
./egnyte.sh $config_ssh_private_key_path /root/.ssh/pdi_cluster_rsa
chmod 700 /root/.ssh/pdi_cluster_rsa 
ssh-add /root/.ssh/pdi_cluster_rsa