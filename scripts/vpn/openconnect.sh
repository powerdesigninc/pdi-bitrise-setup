#!/bin/bash
set -e

# decrypt vpn cred to username/password
eval `node ./crypto-credential.mjs print $config_vpn_cred_path VPN_USERNAME VPN_PASSWORD`

SYSTEM_INFO=`cat /etc/*-release`
if [[ $SYSTEM_INFO =~ "DISTRIB_RELEASE=22.04" ]] ; then
  # install openconnect
  apt-get -y install openconnect >/dev/null 2>&1

  echo "$VPN_PASSWORD" | openconnect \
    --protocol=fortinet -u $VPN_USERNAME \
    --servercert pin-sha256:xTfEl6uMQJET4m7IV1POt76XV9KYBSwMlZKL0Zvx0h8= \
    --passwd-on-stdin  $config_vpn_server &

elif [[ $SYSTEM_INFO =~ "DISTRIB_RELEASE=20.04" ]] ; then
  # TODO: openconnect on 20.04 doesn't support fortinet, so we built a custom package for that
  # Delete these code after all workflows upgrede to 22.04

  # install openconnect deps
  apt-get -y install vpnc-scripts libproxy-dev >/dev/null 2>&1

  $SCRIPT_DIR/egnyte.sh /Shared/IT/Applications/CICD/openconnect-20.04.zip /openconnect.zip

  unzip /openconnect.zip -d / 1>/dev/null

  echo "$VPN_PASSWORD" | LD_LIBRARY_PATH=/openconnect \
    /openconnect/openconnect \
      --protocol=fortinet -u $VPN_USERNAME \
      --servercert pin-sha256:xTfEl6uMQJET4m7IV1POt76XV9KYBSwMlZKL0Zvx0h8= \
      --passwd-on-stdin  $config_vpn_server &
else
  echo "Unsupported OS version"
  exit 1
fi