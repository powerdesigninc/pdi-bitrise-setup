#!/bin/bash
set -e

# check vpn connection

HOST_PING=docker.powerdesigninc.us

apt-get install -y iputils-ping 1>/dev/null

TRYCOUNT=1
while true; do

 sleep 5
 echo "[$TRYCOUNT] try to connect $HOST_PING"

 if [ $TRYCOUNT -ge 10 ]; then
  echo -e "\e[31mVPN connect failed\e[0m";
  exit 1
 fi

 TRYCOUNT=$[ $TRYCOUNT + 1 ]

 ping -c1 $HOST_PING > /dev/null && \
 echo -e "\e[34mVPN connect succeeded\e[0m" && break

done
