set -e

echo -e "\e[32m# Start setup database\e[0m"

echo "Downloading instant client and sqlplus"
apt-get install -y libaio1 1>/dev/null

# download oracle instant client and sql plus

mkdir /opt/oracle
curl -LSsf --output instantclient.zip https://download.oracle.com/otn_software/linux/instantclient/211000/instantclient-basiclite-linux.x64-21.1.0.0.0.zip
unzip -d /opt/oracle instantclient.zip 1>/dev/null
rm instantclient.zip

curl -LSsf --output sqlplus.zip https://download.oracle.com/otn_software/linux/instantclient/211000/instantclient-sqlplus-linux.x64-21.1.0.0.0.zip
unzip -d /opt/oracle sqlplus.zip 1>/dev/null
rm sqlplus.zip

ln -s /opt/oracle/instantclient_21_1/sqlplus /bin/sqlplus

echo /opt/oracle/instantclient_21_1 > /etc/ld.so.conf.d/oracle-instantclient.conf

ldconfig 1>/dev/null

echo "Seting envs"

# set database environments variables
envman add --key DB_CONN_STR_DEV --value laboradb02.powerdesigninc.us:1536/ins02
envman add --key DB_CONN_STR_TEST --value laboradb03.powerdesigninc.us:1538/ins03
envman add --key DB_CONN_STR_UAT --value laboradb04.powerdesigninc.us:1532/ins04
envman add --key DB_CONN_STR_PROD --value pdivlorcldb.powerdesigninc.us:1546/GOLD

# download/decrypt database cert
eval `node ./crypto-credential.mjs print $config_db_cred_path DB_PASSWORD_NONPROD`

# download/decrypt atlassian credential
eval `node ./crypto-credential.mjs print $config_atlassian_cred_path ATLASSIAN_USER ATLASSIAN_PASSWORD`

# download/decrypt ghostinspector credential
eval `node ./crypto-credential.mjs print $config_ghostinspector_cred_path GHOSTINSPECTOR_API_KEY`

# download/decrypt SMTP credential
eval `node ./crypto-credential.mjs print $config_smtp_cred_path SMTP_USERNAME SMTP_PASSWORD`