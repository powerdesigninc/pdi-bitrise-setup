# PDI Bitrise Setup

This project is our costume step for bitrise that help us simplified establishing building environments.

# Usage

1. Create `EGNYTE_ACCESS_TOKEN` on bitrise secrets, this step needs to access egnyte to download files.

2. Unfortunately, Bitrise Workflow Editor doesn't support editing private step when the script was written. we have to use bitrise.yml editor to add the step. The below is a snippet of adding the step into bitrise.yml

```yaml
workflows:
  test:
    steps:
      - git::https://bitbucket.org/powerdesigninc/pdi-bitrise-setup.git@main:
          title: Setup
          inputs:
            - use_pdi_docker_cluster: 'yes'
```

> `@main` can be replaced to a branch or tag. main is the production branch and use it, the workflow will always use latest version, but you can switch to old version if you want to.

# Inputs

| name                        | default value                                                         |
| --------------------------- | --------------------------------------------------------------------- |
| config_egnyte_access_token  | $EGNYTE_ACCESS_TOKEN                                                  |
| use_pdi_vpn                 | yes                                                                   |
| use_pdi_docker_cluster      | no                                                                    |
| use_pdi_database            | no                                                                    |
| config_vpn_server           | vpn.powerdesigninc.us:10443                                           |
| config_vpn_package          | /Shared/IT/Applications/CICD/openconnect-{{version}}.zip              |
| config_vpn_cred_path        | $VPN_CERT_PATH or /Shared/IT/Applications/CICD/credentials/vpn        |
| config_docker_cred_path     | $DOCKER_CERT_PATH or /Shared/IT/Applications/CICD/credentials/docker  |
| config_db_cred_path         | $DB_CERT_PATH or /Shared/IT/Applications/CICD/credentials/db          |
| config_atlassian_cred_path  | $DB_CERT_PATH or /Shared/IT/Applications/CICD/credentials/atlassian   |
| config_ssh_private_key_path | $SSH_PRIVATE_KEY_PATH or /Shared/IT/Applications/CICD/ssh_private_key |

see [step.yml](./step.yml) for more detail.

# Test Locally

Bitrise runs the workflow in docker with its images like `bitriseio/docker-bitrise-base`. We can do the same thing to emulator its running environment.

```bash
export EGNYTE_ACCESS_TOKEN=<the key>
docker run --privileged --rm -v $PWD/:/bitrise-setup -v $PWD/bitrise.yml:/bitrise/src/bitrise.yml -e EGNYTE_ACCESS_TOKEN=$EGNYTE_ACCESS_TOKEN bitriseio/bitrise-base-20.04 bitrise run test
```

# Cert Files

docker and vpn cert files is username/password for each line and encrypted by AES with egnyte_access_token, so if the egnyte_access_token is changed, you have to update these cert files as well.
You can use these command to encrypt the file

```bash
export EGNYTE_ACCESS_TOKEN=<the key>

cat << EOF | node ./scripts/crypto-credential.mjs encrypt
<username>
<password>
EOF

// or output to file directly
cat << EOF | node ./scripts/crypto-credential.mjs encrypt >> file.txt
<username>
<password>
EOF
```
